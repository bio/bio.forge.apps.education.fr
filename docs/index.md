# Ressources pédagogiques de biosciences

## Les exercices interactifs

!!! example "Génétique"

    
    === "Les exercices"

        ![Capture d'écran des exercices sur l'hérédité humaine](assets/capture_ecran_exos_heredite.png "Capture d'écran des exercices sur l'hérédité humaine"){ width=50%; align=right }

        Ces exercices ont pour objectif de faciliter l'entraînement sur les notions d'hérédité dans le cadre des apprentissages en génétique humaine.

        Ces pages (exercices guidés et solutions) s'avèrent extrèmement pratiques dans le cadre de l'apprentissage des principales lois de l'hérédité.

        [Exercices de génétique humaine](https://bio.forge.apps.education.fr/heredite){ .md-button target="_blank" rel="noopener" }

    
    === "Sources"

        !!! note inline end "Extrait du code"

            ``` html
            L'allèle responsable de la phénylcétonurie est
			<select  class="form-select" NAME="DomRec">
				<option VALUE="Nul">Cliquez ici
				<option>Récessif
				<option>Dominant
			</select>
			```

        Ces exercices sont conçus pour être simples au niveau de l'expérience utilisateur, mais aussi simples au niveau du code utilisé (HTML + JavaScript).

        Le code peut ainsi être repris simplement pour créer de nouveaux exercices similaires sur d'autres arbres généalogiques (pour des corrections de sujets d'examen par exemple).

        [sources sur #LaForgeEdu](https://forge.apps.education.fr/bio/heredite){ .md-button target="_blank" rel="noopener" }

## Les applications

!!! example "PDF & Multimedia Viewer"

    
    === "L'application"

        ![Capture d'écran de la webapp PDFmultiViewer](assets/ecran_principal_PDFmultiviewer.png "Capture d'écran de la webapp PDFmultiViewer"){ width=50%; align=right }

        La WebApp « PDF & Multimedia Viewer » permet de lire des paquets regroupant des supports à la fois textuels et multimédia.
        

        [La WebApp PDF & Multimedia Viewer](https://bio.forge.apps.education.fr/pdfmultiviewer/){ .md-button target="_blank" rel="noopener" }

    
    === "Sources"

        !!! note inline end "Extrait du code"

            ``` html
            <!-- Visionneur PDF + Vidéo -->
            <div class="col-lg-10 viewer-column">
                <iframe id="pdf-viewer" class="w-100"></iframe>
                <div id="video-container">
                    <button id="close-video" class="btn btn-danger position-absolute top-0 end-0 m-2">
                        <i class="bi bi-x-lg"></i>
                    </button>
                    <video id="video-player" controls></video>
                </div>
            </div>
			```

        Cette WebApp est très simple (page html + scripts JS). Elle lit les archives .zip que vous aurez sur votre disque dur d'ordinateur (sauf pour les vidéos YouTube qui sont hébergées ailleurs)

        [sources sur #LaForgeEdu](https://forge.apps.education.fr/bio/pdfmultiviewer){ .md-button target="_blank" rel="noopener" }



## Les tutoriels

!!! example "ANKI"

    === "Les tutoriels"

        ![Réviser efficacement avec Anki](https://bio.forge.apps.education.fr/anki/assets/images/choisir_anki_pour_reviser.png){ width=50%; align=right }

        Trois tutoriels qui se complètent pour être immédiatement opérationnel dans ses révisions grâce à ANKI.

        Des exemples d'applications pédagogiques en classe sont également présentés.
    
        [Tutoriels sur ANKI](https://bio.forge.apps.education.fr/anki){ .md-button target="_blank" rel="noopener" }

        
    === "Sources"

        !!! note inline end "Extrait du code"

            ``` markdown
            ## Les trois modèles de cartes
            
            !!! example "Le modèle «**Quiz**»"
                ![](./documents/quiz.png){ width=30%; align=right }

            - Les cartes «**Question/Réponse**» doivent utiliser les [verbes d'actions](./documents/Glossaire_verbes_d_action_420261.pdf) pour la question.
            ```



        L'ensemble des vidéos présentées est hébergé sur la [chaîne BioSciences](https://tube-sciences-technologies.apps.education.fr/c/hardouin_patrice_channel).

        Ces exercices sont conçus pour être simples au niveau de l'expérience utilisateur, mais aussi simples au niveau du code utilisé (Markdown).

        Le code peut ainsi être repris simplement pour créer de nouveaux tutoriels similaires.

    
        [sources sur #LaForgeEdu](https://forge.apps.education.fr/bio/anki){ .md-button target="_blank" rel="noopener" }

## Les activités pédagogiques        

!!! example "Activités minute"

    === "Les fiches d'activité"

        ![Les activités minute](https://bio.forge.apps.education.fr/minute/res/stopwatch.jpg "Les activités minute"){ width=50%; align=right }    

        Des activités rapides pour bien prendre en main ses cours : de la case de bande dessinée, au podcast, en passant par toute une variété d'activités permettant de mobiliser des compétences différentes et complémentaires
        
        Treize fiches d'activités rapides, à piocher. Le temps volontairement contraint de ces activités oblige les apprenants à aller directement à l'essentiel.

        Pratique pour proposer des activités modulaires par îlots ou par petits groupes. Certaines fiches peuvent également être utilisées pour lancer une activité commune à tous les apprenants d'une même classe.

        [Activités minute](https://bio.forge.apps.education.fr/minute){ .md-button target="_blank" rel="noopener" }

    === "Sources"

        ![Les activités minute](https://bio.forge.apps.education.fr/minute/res/stopwatch.jpg "Les activités minute"){ width=50%; align=right }  

        [sources sur #LaForgeEdu](https://forge.apps.education.fr/bio/minute){ .md-button target="_blank" rel="noopener" }
    


## Les cours sous LaTeX

??? example "les molécules du vivant"

    !!! warning "en cours de mise en place sur #LaForgeEdu"

        [Bientôt : Les molécules du vivant](https://forge.apps.education.fr/bio/biochimie)
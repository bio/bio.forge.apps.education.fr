---
author: Patrice HARDOUIN
title: Crédits
---

Le site est hébergé par la [*#LaForgeEdu*](https://forge.apps.education.fr/bio/).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).



